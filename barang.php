<?php
include "include/header.php";
?>

<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="barang.php" class="current">Barang</a></div>
    <h1>Barang</h1>
  </div>

<div class="container-fluid">
	<hr>
	<div class="row-fluid">
		<p align="center">
			<a href="tambah_barang.php" type="button" class="btn btn-primary"><i class="icon-plus"></i></a>
		</p>
		<p align="right">
			<a href="export/export_excel_barang.php" type="button" class="btn btn-success">Export Excel</a>
			<a href="export/cetak_barang.php" type="button" class="btn btn-danger">Cetak</a>
		</p>

			<?php
			$modal = mysql_query("SELECT i.*,r.nama_ruang,j.nama_jenis,p.nama_petugas FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas");
			while($aw = mysql_fetch_array($modal))
			{
				?>
				<div class="modal fade" id="myModal<?=$aw['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h3><b>Data Lengkap Barang</b></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 7px;">&times;</button>
							</div>
							<div class="modal-body">
								<div class="table-responsive  ">
									<table id="example3">
										<thead>

										</thead>
										<tbody>
											<?php
											$data =mysql_query("SELECT i.*,r.nama_ruang,j.nama_jenis,p.nama_petugas FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas where id_inventaris='$aw[id_inventaris]'");
											$no=1;
											while ($ra=mysql_fetch_array($data))
											{
												echo"<tr>";
												echo'<th>Nama Barang</th>';
												echo"<td>:</td>";
												echo "<td>$ra[nama]</td>";
												echo'</tr>';
												echo"<tr>";
												echo'<th>Merk Barang</th>';
												echo"<td>:</td>";
												echo "<td>$ra[merk]</td>";
												echo'</tr>';
												echo"<tr>";
												echo '<th>Kondisi</th>';
												echo"<td>:</td>";
												echo "<td>$ra[kondisi]</td>";
												echo"</tr>";

												echo"<tr>";
												echo '<th>Keterangan</th>';
												echo"<td>:</td>";
												echo "<td>$ra[keterangan]</td>";
												echo"</tr>";

												echo"<tr>";
												echo '<th>Jumlah</th>';	
												echo"<td>:</td>";
												echo "<td>$ra[jumlah]</td>";
												echo"</tr>";

												echo"<tr>";
												echo '<th>Jenis</th>';
												echo"<td>:</td>";
												echo "<td>$ra[nama_jenis]</td>";
												echo"</tr>";

												echo"<tr>";
												echo'<th>Tanggal register</th>';
												echo"<td>:</td>";
												echo "<td>$ra[tanggal_register]</td>";
												echo"</tr>";

												echo"<tr>";
												echo'<th>Nama ruang</th>';
												echo"<td>:</td>";
												echo "<td>$ra[nama_ruang]</td>";
												echo"</tr>";

												echo"<tr>";
												echo'<th>Kode inventaris</th>';
												echo"<td>:</td>";
												echo "<td>$ra[kode_inventaris]</td>";
												echo"</tr>";

												echo"<tr>";
												echo'<th>Petugas</th>';	
												echo"<td>:</td>";
												echo "<td>$ra[nama_petugas]</td>";
												echo"</tr>";

												$no++;
											}
											?>
										</tbody>
									</table>
								</div>		
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			?>

			<div style="clear:both"></div>
			<div class="widget-box">
				<div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
				   <h5>Barang</h5>
				</div>
					<div class="widget-content nopadding">
						<table class="table table-striped table-bordered table-responsive data-table">
							<thead>
								<tr>
									<th>No</th>
									<th>Foto</th>
									<th>Nama Barang</th>
									<th>Merk Barang</th>
									<th>Jumlah</th>
									<th>Tanggal Register</th>
									<th>Nama Ruang</th>
									<th>Petugas</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no=1;
								$data=mysql_query("SELECT i.*,r.nama_ruang,j.nama_jenis,p.nama_petugas FROM inventaris i JOIN ruang r ON i.id_ruang=r.id_ruang JOIN jenis j ON i.id_jenis=j.id_jenis JOIN petugas p ON i.id_petugas=p.id_petugas");
								while($ba=mysql_fetch_array($data)) {
									echo"<tr>
									<td class='text-center'>$no</td>
									<td class='text-center'><img src=img/$ba[foto] width=100px ></td>
									<td class='text-center'>$ba[nama]</td>
									<td class='text-center'>$ba[merk]</td>
									<td class='text-center'>$ba[jumlah]</td>
									<td class='text-center'>$ba[tanggal_register]</td>
									<td class='text-center'>$ba[nama_ruang]</td>
									<td class='text-center'>$ba[nama_petugas]</td>
									<td>
									<button class='btn btn-success btn-circle' data-toggle='modal' data-target='#myModal$ba[id_inventaris]'><i class='icon icon-eye-open'></i></button>
									<a href='edit_barang.php?id_inventaris=$ba[id_inventaris]' class='btn btn-info'><i class='fa fa-edit'></i> Edit</a>
									<a href='hapus_barang.php?id_inventaris=$ba[id_inventaris]' class='btn btn-danger'><i class='fa fa-trash'></i> Hapus</a>
									</td>
									</tr>";$no++;
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
	</div>
</div>






	<?php
	include "include/footer.php";
	?>