<?php
include "../koneksi.php";

header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data-pengembalian.xls");
 
// Tambahkan table
?>

<p align="center" id="title-laporan">DATA PENGEMBALIAN</p><br>
<a type="button" value="EXCEL" href="" id="cetak" class="no-print"> </a>

<table border="1" width="100%" style="border-collapse: collapse;">
	<thead class="title-table">
		<tr style="height: 40px;">
		  <th>No</th>
      <th>Nama Peminjam</th>
      <th>Kode Peminjam</th>
      <th>Nama Barang</th>
      <th>Status Peminjaman</th>
      <th>Jumlah</th>
		</tr>
	</thead>
	<tbody>
		 <?php
          $no=1;

          $sql=mysql_query("SELECT * FROM peminjaman p LEFT JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN peminjam pm ON p.id_peminjam=pm.id_peminjam LEFT JOIN inventaris i ON i.id_inventaris=i.id_inventaris ORDER BY p.id_peminjaman DESC");
          while($data=mysql_fetch_array($sql)){
            ?>
          <tr>
          <td class='text-center'><?=$no?></td>
          <td class='text-center'><?=$data['nama_peminjam']?></td>
          <td class='text-center'><?=$data['kode_peminjaman']?></td>
          <td class='text-center'><?=$data['nama']?></td>
          <td class='text-center'><?=$data['status_peminjaman']?></td>
          <td class='text-center'><?=$data['jumlah']?></td>
          <td class='text-center'>
            <?php
            if ($data['status_peminjaman']=='Sedang di pinjam') {
              ?>
              <a href="proses_kembalikan_siswa.php?id_peminjam=<?=$data['id_peminjam']?>&id_peminjaman=<?=$data['id_peminjaman']?>&id_inventaris=<?=$data['id_inventaris']?>&jumlah=<?=$data['jumlah']?>" class='btn btn-danger'><i class='fa fa-trash'></i> Kembalikan</a>
            <?php
            }else{
              echo "Telah Kembali";

            }

            ?>
            
          </td>
        </tr>
        <?php
        $no++;
          }

          ?>
          
	</tbody>
</table>



