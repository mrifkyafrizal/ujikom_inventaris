<?php
include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="jenis.php" class="current">Jenis</a></div>
    <h1>Jenis</h1>
  </div>
<div class="container-fluid">
  <hr>
  <div class="row-fluid">
    <p align="center">
      <a href="tambah_jenis.php" type="button" class="btn btn-primary"><i class="icon-plus"></i></a>
    </p>
    <p align="right">
      <a href="export/export_excel_jenis.php" type="button" class="btn btn-success">Export Excel</a>
      <a href="export/cetak_jenis.php" type="button" class="btn btn-danger">Cetak</a>
    </p>
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
          <h5>Jenis</h5>
      </div>
   <table class="table table-striped table-bordered table-responsive data-table">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Jenis</th>
        <th>Kode Jenis</th>
        <th>Keterangan</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      $rn=mysql_query("SELECT * FROM jenis ");
      while($min=mysql_fetch_array($rn)) {
        echo "<tr>
        <td class='text-center'>$no</td>
        <td class='text-center'>$min[nama_jenis]</td>
        <td class='text-center'>$min[kode_jenis]</td>
        <td class='text-center'>$min[keterangan]</td>
        <td class='text-center'>
        <a href='edit_jenis.php?id_jenis=$min[id_jenis]' class='btn btn-info'><i class='fa fa-edit'></i> Edit</a>
        <a href='hapus_jenis.php?id_jenis=$min[id_jenis]' class='btn btn-danger'><i class='fa fa-trash'></i> Hapus</a>
        </td>
        </tr>";$no++;
      }
      ?>
    </tbody>
  </table>
</div>
</div>
</div>
</div>
<!--End-Action boxes-->    
<?php
include"include/footer.php";
?>