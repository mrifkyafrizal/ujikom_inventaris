<?php
  include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="ruang.php" class="tip-bottom">Ruang</a><a href="tambah_ruang.php" class="current">Tambah ruang</a></div>
    <h1>Tambah ruang</h1>
  </div>

  <div class="container-fluid">
    <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Form Ruang</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" method="POST" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Nama Ruang :</label>
              <div class="controls">
                <input type="text" name="nama_ruang" class="span11" placeholder="Nama Ruang" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Kode Ruang :</label>
              <div class="controls">
                <input type="text" name="kode_ruang" class="span11" placeholder="Kode Ruang" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Keterangan :</label>
              <div class="controls">
                <input type="text" name="keterangan" class="span11" placeholder="Keterangan" required />
              </div>
            </div>
            <div class="form-actions" style="text-align: right;">
              <a href="ruang.php" type="button" class="btn btn-danger">Cancel</a>
              <input type="submit" name="submit" class="btn btn-success" value="Submit" />
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--End-Action boxes-->  

<?php
  if(isset($_POST['cancel']))
    echo "<script>window.location.assign('ruang.php')</script>";
?>

<?php
include"./koneksi.php";
  if(isset($_POST['submit']))
{
  $nama_ruang= $_POST['nama_ruang'];
  $kode_ruang= $_POST['kode_ruang'];
  $keterangan= $_POST['keterangan'];
  $input = mysql_query("INSERT INTO ruang (nama_ruang,kode_ruang,keterangan) VALUES ('$nama_ruang','$kode_ruang','$keterangan')");
  if($input){
    echo "<script>window.location.assign('ruang.php')</script>";
  }else{
    echo "gagal";
  }
}
?>

<?php
  include"include/footer.php";
?>
