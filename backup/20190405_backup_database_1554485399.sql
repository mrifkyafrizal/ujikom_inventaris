DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `id_peminjaman` varchar(100) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("178","KD1935Apr","35","1","27");
INSERT INTO detail_pinjam VALUES("179","KD1935Apr","35","1","28");
INSERT INTO detail_pinjam VALUES("180","KD1934Apr","34","1","29");
INSERT INTO detail_pinjam VALUES("181","KD1935Apr","35","1","30");
INSERT INTO detail_pinjam VALUES("182","KD1935Apr","35","1","31");
INSERT INTO detail_pinjam VALUES("183","KD1934Apr","34","1","32");
INSERT INTO detail_pinjam VALUES("184","KD1935Apr","35","1","33");
INSERT INTO detail_pinjam VALUES("185","KD1935Apr","35","1","34");
INSERT INTO detail_pinjam VALUES("186","KD1934Apr","34","1","35");
INSERT INTO detail_pinjam VALUES("187","KD1934Apr","34","1","36");
INSERT INTO detail_pinjam VALUES("188","KD1934Apr","34","1","37");
INSERT INTO detail_pinjam VALUES("189","KD1934Apr","34","1","38");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL AUTO_INCREMENT,
  `foto` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("34","laptop2.jpg","Laptop","Acer01","Sangat Baik","Untuk semua","3","1","2019-04-05 15:41:38","1","123","2");
INSERT INTO inventaris VALUES("35","bola.jpg","Bola","Nike01","Sangat baik","Untuk semua","3","1","2019-04-05 14:30:16","1","123r","2");
INSERT INTO inventaris VALUES("36","laptop2.jpg","Laptop","Acer02","Baik","Untuk semua","2","1","2019-04-05 13:52:16","1","213","2");
INSERT INTO inventaris VALUES("37","","panahan","zzz","baik","zzz","24","1","2019-04-04 09:23:38","1","zzzz","2");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Alat Elektronik","e12","Untuk semua");
INSERT INTO jenis VALUES("2","Alat Olahraga","o2","Untuk semua");
INSERT INTO jenis VALUES("4","Alat otomotif","ot10","Untuk jurusan TKR");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` text NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Miswan Wahyudi","10203","Ciomas","1");



DROP TABLE peminjam;

CREATE TABLE `peminjam` (
  `id_peminjam` int(15) NOT NULL AUTO_INCREMENT,
  `nisn` int(50) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `id_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_peminjam`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO peminjam VALUES("1","1222011","Muhammad","XII RPL 3","3");
INSERT INTO peminjam VALUES("3","1122100","Lutfi","XII RPL 3","3");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  `id_peminjam` int(15) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `peminjaman_ibfk_1` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("27","KD1935Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("28","KD1935Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("29","KD1934Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("30","KD1935Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","0","1");
INSERT INTO peminjaman VALUES("31","KD1935Apr","2019-04-05","2019-04-05","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("32","KD1934Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("33","KD1935Apr","2019-04-05","2019-04-05","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("34","KD1935Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("35","KD1934Apr","2019-04-05","2019-04-05","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("36","KD1934Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("37","KD1934Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","0","1");
INSERT INTO peminjaman VALUES("38","KD1934Apr","2019-04-05","2019-04-05","Telah Dikembalikan","0","0","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_key` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("2","admin","10b083dd59d8ac9834250f397bc55bb9","mrifkyafrizal@gmail.com","34bea3159a680b703d26f4bf75040e1e5ca6c975df788","Rifky","1","1");
INSERT INTO petugas VALUES("6","superadmin","17c4520f6cfd1ab53d8745e84681eb49","mrifkyafrizal@gmail.com","","Afrizal","1","1");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","rpl1","Untuk kelas rpl 1");
INSERT INTO ruang VALUES("2","Lab RPL 2","rpl2","Untuk kelas rpl 2");
INSERT INTO ruang VALUES("4","Lapangan","lp1","Untuk semua jurusan");



