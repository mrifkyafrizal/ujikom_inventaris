DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `id_peminjaman` varchar(100) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("167","KD1936Apr","36","1","");
INSERT INTO detail_pinjam VALUES("168","KD1934Apr","34","1","18");
INSERT INTO detail_pinjam VALUES("169","KD1934Apr","34","1","19");
INSERT INTO detail_pinjam VALUES("170","KD1935Apr","35","1","20");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL AUTO_INCREMENT,
  `foto` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("34","laptop2.jpg","Laptop","Acer01","Sangat Baik","Untuk semua","5","1","2019-04-04 13:39:33","1","123","2");
INSERT INTO inventaris VALUES("35","bola.jpg","Bola","Nike01","Sangat baik","Untuk semua","3","1","2019-04-04 14:12:18","1","123r","2");
INSERT INTO inventaris VALUES("36","laptop2.jpg","Laptop","Acer02","Baik","Untuk semua","3","1","2019-04-04 13:37:41","1","213","2");
INSERT INTO inventaris VALUES("37","","panahan","zzz","baik","zzz","24","1","2019-04-04 09:23:38","1","zzzz","2");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Alat Elektronik","e12","Untuk semua");
INSERT INTO jenis VALUES("2","Alat Olahraga","o2","Untuk semua");
INSERT INTO jenis VALUES("4","Alat otomotif","ot10","Untuk jurusan TKR");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` text NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Miswan W","10203","Bogor","2");



DROP TABLE peminjam;

CREATE TABLE `peminjam` (
  `id_peminjam` int(15) NOT NULL AUTO_INCREMENT,
  `nama_peminjam` varchar(100) NOT NULL,
  `nisn` int(50) NOT NULL,
  `level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_peminjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `peminjaman_ibfk_1` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("18","KD1934Apr","2019-04-04","2019-04-04","Telah Dikembalikan","0","1");
INSERT INTO peminjaman VALUES("19","KD1934Apr","2019-04-04","2019-04-04","Telah Dikembalikan","2","0");
INSERT INTO peminjaman VALUES("20","KD1935Apr","2019-04-04","0000-00-00","Sedang di pinjam","0","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_key` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("2","admin","10b083dd59d8ac9834250f397bc55bb9","mrifkyafrizal@gmail.com","","Rifky","1","1");
INSERT INTO petugas VALUES("6","superadmin","17c4520f6cfd1ab53d8745e84681eb49","mrifkyafrizal@gmail.com","","Afrizal","1","1");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","rpl1","Untuk kelas rpl 1");
INSERT INTO ruang VALUES("2","Lab RPL 2","rpl2","Untuk kelas rpl 2");
INSERT INTO ruang VALUES("4","Lapangan","lp1","Untuk semua jurusan");



