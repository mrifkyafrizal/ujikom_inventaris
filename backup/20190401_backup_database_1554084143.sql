DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `id_peminjaman` varchar(100) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("12","KD1934Mar","34","1","42");
INSERT INTO detail_pinjam VALUES("13","KD1934Mar","34","1","43");
INSERT INTO detail_pinjam VALUES("14","KD1935Mar","35","1","44");
INSERT INTO detail_pinjam VALUES("15","KD1934Apr","34","1","45");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL AUTO_INCREMENT,
  `foto` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("34","laptop2.jpg","Laptop","Acer01","Sangat Baik","Untuk semua","4","1","2019-04-01 08:50:43","1","123","2");
INSERT INTO inventaris VALUES("35","bola.jpg","Bola","Nike01","Sangat baik","Untuk semua","8","2","2019-03-31 11:12:36","4","123r","2");
INSERT INTO inventaris VALUES("36","laptop2.jpg","Laptop","Acer02","Baik","Untuk semua","10","1","0000-00-00 00:00:00","1","213","2");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Alat Elektronik","e12","Untuk semua");
INSERT INTO jenis VALUES("2","Alat Olahraga","o2","Untuk semua");
INSERT INTO jenis VALUES("4","Alat otomotif","ot10","Untuk jurusan TKR");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` int(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Drs.Miswan Wahyudi, MM","19670601","","admin","","0");



DROP TABLE peminjam;

CREATE TABLE `peminjam` (
  `id_peminjam` int(15) NOT NULL AUTO_INCREMENT,
  `nama_peminjam` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_peminjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `peminjaman_ibfk_1` (`id_petugas`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("42","KD1934Mar","2019-03-31","2019-03-31","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("43","KD1934Mar","2019-03-31","2019-03-31","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("44","KD1935Mar","2019-03-31","2019-03-31","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("45","KD1934Apr","2019-04-01","2019-04-01","Telah Dikembalikan","2");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_key` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("2","admin","21232f297a57a5a743894a0e4a801fc3","mrifkyafrizal@gmail.com","a470700b28535fe78ede4870e117c0795ca16eb0e4f06","Rifky","1","1");
INSERT INTO petugas VALUES("4","superadmin","superadmin","","","Afrizal","0","2");
INSERT INTO petugas VALUES("5","rifky","afrizal","mrifkyafrizal@gmail.com","","afrizal","1","2");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","rpl1","Untuk kelas rpl 1");
INSERT INTO ruang VALUES("2","Lab RPL 2","rpl2","Untuk kelas rpl 2");
INSERT INTO ruang VALUES("4","Lapangan","lp1","Untuk semua jurusan");



