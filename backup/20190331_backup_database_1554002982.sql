DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `id_peminjaman` varchar(100) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("1","KD1934Mar","34","1","28");
INSERT INTO detail_pinjam VALUES("2","KD1934Mar","34","1","29");
INSERT INTO detail_pinjam VALUES("3","KD1934Mar","34","1","30");
INSERT INTO detail_pinjam VALUES("4","KD1934Mar","34","1","31");
INSERT INTO detail_pinjam VALUES("5","KD1934Mar","34","1","32");
INSERT INTO detail_pinjam VALUES("6","KD1934Mar","34","1","33");
INSERT INTO detail_pinjam VALUES("7","KD1934Mar","34","1","34");
INSERT INTO detail_pinjam VALUES("8","KD1935Mar","35","1","35");
INSERT INTO detail_pinjam VALUES("9","KD1934Mar","34","1","36");
INSERT INTO detail_pinjam VALUES("10","KD1934Mar","34","3","37");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL AUTO_INCREMENT,
  `foto` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("34","laptop2.jpg","Laptop","Acer01","Sangat Baik","Untuk semua","5","1","2019-03-31 10:28:51","1","123","2");
INSERT INTO inventaris VALUES("35","bola.jpg","Bola","Nike01","Sangat baik","Untuk semua","8","2","2019-03-30 23:14:17","4","123r","2");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Alat Elektronik","e12","Untuk semua");
INSERT INTO jenis VALUES("2","Alat Olahraga","o2","Untuk semua");
INSERT INTO jenis VALUES("4","Alat otomotif","ot10","Untuk jurusan TKR");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` int(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Drs.Miswan Wahyudi, MM","19670601","admin","","0");



DROP TABLE peminjam;

CREATE TABLE `peminjam` (
  `id_peminjam` int(15) NOT NULL AUTO_INCREMENT,
  `nama_peminjam` varchar(100) NOT NULL,
  `level` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_peminjam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `peminjaman_ibfk_1` (`id_petugas`),
  CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("14","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("15","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("16","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("17","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("18","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("19","KD1923Mar","2019-03-08","0000-00-00","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("20","KD1923Mar","2019-03-08","0000-00-00","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("21","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("22","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("23","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("24","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("25","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("26","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("27","KD1923Mar","2019-03-08","2019-03-08","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("28","KD1934Mar","2019-03-09","2019-03-09","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("29","KD1934Mar","2019-03-09","2019-03-09","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("30","KD1934Mar","2019-03-09","2019-03-09","Telah Dikembalikan","4");
INSERT INTO peminjaman VALUES("31","KD1934Mar","2019-03-10","2019-03-10","Telah Dikembalikan","4");
INSERT INTO peminjaman VALUES("32","KD1934Mar","2019-03-30","2019-03-30","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("33","KD1934Mar","2019-03-30","2019-03-30","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("34","KD1934Mar","2019-03-30","2019-03-30","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("35","KD1935Mar","2019-03-30","2019-03-30","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("36","KD1934Mar","2019-03-31","2019-03-31","Telah Dikembalikan","2");
INSERT INTO peminjaman VALUES("37","KD1934Mar","2019-03-31","0000-00-00","Sedang di pinjam","2");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_key` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("2","admin","admin","mrifkyafrizal@gmail.com","","Rifky","0","1");
INSERT INTO petugas VALUES("4","superadmin","superadmin","","","Afrizal","0","2");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","rpl1","Untuk kelas rpl 1");
INSERT INTO ruang VALUES("2","Lab RPL 2","rpl2","Untuk kelas rpl 2");
INSERT INTO ruang VALUES("4","Lapangan","lp1","Untuk semua jurusan");



