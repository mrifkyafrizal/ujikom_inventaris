DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `id_inventaris` int(15) NOT NULL,
  `jumlah` int(25) NOT NULL,
  `id_peminjaman` varchar(100) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("178","KD1935Apr","35","1","27");
INSERT INTO detail_pinjam VALUES("179","KD1935Apr","35","1","28");
INSERT INTO detail_pinjam VALUES("180","KD1934Apr","34","1","29");
INSERT INTO detail_pinjam VALUES("181","KD1935Apr","35","1","30");
INSERT INTO detail_pinjam VALUES("182","KD1935Apr","35","1","31");
INSERT INTO detail_pinjam VALUES("183","KD1934Apr","34","1","32");
INSERT INTO detail_pinjam VALUES("184","KD1935Apr","35","1","33");
INSERT INTO detail_pinjam VALUES("185","KD1935Apr","35","1","34");
INSERT INTO detail_pinjam VALUES("186","KD1934Apr","34","1","35");
INSERT INTO detail_pinjam VALUES("187","KD1934Apr","34","1","36");
INSERT INTO detail_pinjam VALUES("188","KD1934Apr","34","1","37");
INSERT INTO detail_pinjam VALUES("189","KD1934Apr","34","1","38");
INSERT INTO detail_pinjam VALUES("190","KD1934Apr","34","1","39");
INSERT INTO detail_pinjam VALUES("191","KD1934Apr","34","1","1");
INSERT INTO detail_pinjam VALUES("192","KD1936Apr","36","1","2");
INSERT INTO detail_pinjam VALUES("193","KD1935Apr","35","1","3");
INSERT INTO detail_pinjam VALUES("194","KD1935Apr","35","1","4");
INSERT INTO detail_pinjam VALUES("195","KD1934Apr","34","1","5");
INSERT INTO detail_pinjam VALUES("196","KD1934Apr","34","1","6");
INSERT INTO detail_pinjam VALUES("197","","35","1","7");
INSERT INTO detail_pinjam VALUES("198","","36","1","8");
INSERT INTO detail_pinjam VALUES("199","KD1934Apr","34","1","9");
INSERT INTO detail_pinjam VALUES("200","KD1935Apr","35","1","10");
INSERT INTO detail_pinjam VALUES("201","NT9F24MT","34","1","11");
INSERT INTO detail_pinjam VALUES("202","8WDC75BG","35","1","12");
INSERT INTO detail_pinjam VALUES("203","A0T8Q4JO","34","1","13");
INSERT INTO detail_pinjam VALUES("204","AJU9LBJF","35","1","14");
INSERT INTO detail_pinjam VALUES("205","0D85IKIC","35","1","15");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(15) NOT NULL AUTO_INCREMENT,
  `foto` varchar(25) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `kondisi` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(15) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_ruang` int(15) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`),
  CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`),
  CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("34","laptop2.jpg","Laptop","Acer01","Sangat Baik","Untuk semua","3","1","2019-04-09 11:07:56","1","123","2");
INSERT INTO inventaris VALUES("35","bola.jpg","Bola","Nike01","Sangat baik","Untuk semua","2","1","2019-04-10 09:38:58","1","123r","2");
INSERT INTO inventaris VALUES("36","laptop3.jpg","Laptop","Acer02","Baik","Untuk semua","2","1","2019-04-09 10:13:36","1","213","2");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(15) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Alat Elektronik","e12","Untuk semua");
INSERT INTO jenis VALUES("2","Alat Olahraga","o2","Untuk semua");
INSERT INTO jenis VALUES("4","Alat otomotif","ot10","Untuk jurusan TKR");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(15) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","administrator");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","peminjam");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(15) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","197307072001","Drs.Miswan Wahyudi, MM","Bogor","2");
INSERT INTO pegawai VALUES("5","197307072002","Hajarol Harahap, S.Pd","Bogor","2");
INSERT INTO pegawai VALUES("6","197307072003","Kartanto, S.Pd","Bogor","2");
INSERT INTO pegawai VALUES("7","197307072004","Januar Ashari, S.Pd","Bogor","2");
INSERT INTO pegawai VALUES("8","197307072005","Hasrul Adiputra Harahap S.Kom","Bogor","2");
INSERT INTO pegawai VALUES("9","197307072006","Ahmad Faisol H,S.Pd","Bogor","2");
INSERT INTO pegawai VALUES("10","197307072007","Erwan Usmawan, S.Kom","Bogor","2");
INSERT INTO pegawai VALUES("11","197307072008","Heru Setiawan S.Kom","Bogor","2");



DROP TABLE peminjam;

CREATE TABLE `peminjam` (
  `id_peminjam` int(15) NOT NULL AUTO_INCREMENT,
  `nisn` varchar(15) NOT NULL,
  `nama_peminjam` varchar(100) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `id_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_peminjam`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO peminjam VALUES("4","0005894098","Arman Ramdhani","XII RPL 3","3");
INSERT INTO peminjam VALUES("5","0015312553","Amanda Siti Jubaedah","XII RPL 3","3");
INSERT INTO peminjam VALUES("6","0012357221","Annisa Ulhusnah","XII RPL 3","3");
INSERT INTO peminjam VALUES("7","0012013090","Dandy Junaedy","XII RPL 3","3");
INSERT INTO peminjam VALUES("8","0018635758","Dudi Iskandar","XII RPL 3","3");
INSERT INTO peminjam VALUES("9","0005893954","Muhammad Rifky Afrizal","XII RPL 3","3");
INSERT INTO peminjam VALUES("10","0013574012","Rinaldi Ardiansyah","XII RPL 3","3");
INSERT INTO peminjam VALUES("11","0008597176","Luthfi Ramadhan","XII RPL 3","3");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(15) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(100) NOT NULL,
  `id_petugas` int(15) NOT NULL,
  `id_pegawai` int(15) NOT NULL,
  `id_peminjam` int(15) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `peminjaman_ibfk_1` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","KD1934Apr","2019-04-07","2019-04-07","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("2","KD1936Apr","2019-04-07","2019-04-07","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("3","KD1935Apr","2019-04-07","2019-04-07","Telah Dikembalikan","0","0","4");
INSERT INTO peminjaman VALUES("4","KD1935Apr","2019-04-07","2019-04-07","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("5","KD1934Apr","2019-04-07","2019-04-09","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("6","KD1934Apr","2019-04-09","2019-04-09","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("7","JG6CN71G","2019-04-09","2019-04-09","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("8","W1GP1AX5","2019-04-09","2019-04-09","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("9","KD1934Apr","2019-04-09","2019-04-09","Telah Dikembalikan","0","0","4");
INSERT INTO peminjaman VALUES("10","KD1935Apr","2019-04-09","2019-04-09","Telah Dikembalikan","0","0","4");
INSERT INTO peminjaman VALUES("11","NT9F24MT","2019-04-09","2019-04-09","Telah Dikembalikan","2","0","0");
INSERT INTO peminjaman VALUES("12","8WDC75BG","2019-04-09","2019-04-09","Telah Dikembalikan","0","1","0");
INSERT INTO peminjaman VALUES("13","A0T8Q4JO","2019-04-09","2019-04-09","Telah Dikembalikan","0","0","4");
INSERT INTO peminjaman VALUES("14","AJU9LBJF","2019-04-10","2019-04-10","Telah Dikembalikan","6","0","0");
INSERT INTO peminjaman VALUES("15","0D85IKIC","2019-04-10","0000-00-00","Sedang di pinjam","2","0","0");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_key` varchar(100) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `id_level` int(15) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("2","admin","10b083dd59d8ac9834250f397bc55bb9","mrifkyafrizal@gmail.com","","Rifky","1","1");
INSERT INTO petugas VALUES("6","superadmin","2b474387a481cd9de9fd34910b7e72a6","mrifkyafrizal@gmail.com","","Afrizal","1","1");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(15) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `kode_ruang` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","rpl1","Untuk kelas rpl 1");
INSERT INTO ruang VALUES("2","Lab RPL 2","rpl2","Untuk kelas rpl 2");
INSERT INTO ruang VALUES("4","Lapangan","lp1","Untuk semua jurusan");



