<?php
  include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="pengembalian_siswa.php" class="current">Pengembalian</a></div>
    <h1>Pengembalian</h1>
  </div>

  <div class="container-fluid">
      <hr>
      <div class="row-fluid">
        <p align="right">
          <a href="export/export_excel_pengembalian_siswa.php" type="button" class="btn btn-success">Export Excel</a>
          <a href="export/cetak_pengembalian_siswa.php" type="button" class="btn btn-danger">Cetak</a>
        </p>
        <div class="widget-box">
           <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
             <h5>Table</h5>
           </div>
      <table class="table table-striped table-bordered table-responsive data-table">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Peminjam</th>
            <th>Kode Peminjam</th>
            <th>Nama Barang</th>
            <th>Kelas</th>
            <th>Merk</th>
            <th>Status Peminjaman</th>
            <th>Jumlah</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no=1;
          $where="WHERE p.id_peminjam ";
          $sql=mysql_query("SELECT p.*,pm.nama_peminjam,i.nama,pm.kelas,i.merk,d.jumlah,i.id_inventaris FROM peminjaman p LEFT JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman LEFT JOIN peminjam pm ON p.id_peminjam=pm.id_peminjam LEFT JOIN inventaris i ON d.id_inventaris=i.id_inventaris ".$where." ORDER BY p.id_peminjaman DESC");

          while($data=mysql_fetch_array($sql)){
            ?>
          <tr>
          <td class='text-center'><?=$no?></td>
          <td class='text-center'><?=$data['nama_peminjam']?></td>
          <td class='text-center'><?=$data['kode_peminjaman']?></td>
          <td class='text-center'><?=$data['nama']?></td>
          <td class='text-center'><?=$data['kelas']?></td>
          <td class='text-center'><?=$data['merk']?></td>
          <td class='text-center'><?=$data['status_peminjaman']?></td>
          <td class='text-center'><?=$data['jumlah']?></td>
          <td class='text-center'>
            <?php
            if ($data['status_peminjaman']=='Sedang di pinjam') {
              ?>
              <a href="proses_kembalikan_siswa.php?id_peminjam=<?=$data['id_peminjam']?>&id_peminjaman=<?=$data['id_peminjaman']?>&id_inventaris=<?=$data['id_inventaris']?>&jumlah=<?=$data['jumlah']?>" class='btn btn-danger'><i class='fa fa-trash'></i> Kembalikan</a>
            <?php
            }else{
              echo "Telah Kembali";

            }

            ?>
            
          </td>
        </tr>
        <?php
        $no++;
          }

          ?>
          
        </tbody>
      </table>
    </div>
</div>
</div>
</div>


<!--End-Action boxes-->    
<?php
  include"include/footer.php";
?>