<?php 
include 'inc/base.php';
$id = isset($_GET['id']) ? $_GET['id'] : null;
$user_key = isset($_GET['user_key']) ? $_GET['user_key'] : null;

$sql = "SELECT * FROM tm_anggota WHERE id = '$id' AND user_key='$user_key'";
$query = mysqli_query($conn, $sql);
$row = mysqli_fetch_assoc($query);
if ($row) {
	$status = $row['status'];
	if ($status == '1') {
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal, Akun anda telah teraktivasi!',
		        'type' => 'error',
		        'href' => 'login.php'

		    ];	
	}else{
		$sql = "UPDATE tm_anggota SET status = 1, user_key = '' WHERE id = '$id'";
		$query = mysqli_query($conn,$sql);
		if ($query) {
		    $message = [
		        'title' => 'Sukses!',
		        'txt' => 'Aktivasi Berhasil, Silahkan Login!',
		        'type' => 'success',
		        'href' => 'login.php'
		    ];
		}else{
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal!',
		        'type' => 'error',
		        'href' => 'login.php'
		    ];			
		}
	}
}else{
	$message = [
        'title' => 'Gagal!',
        'txt' => 'Registrasi Gagal, Kode verifikasi salah!',
        'type' => 'error',
		'href' => 'login.php'
    ];	
}

if (isset($message) && $id && $user_key) {
	include ROOT.'inc/_header.php';
	include ROOT.'inc/_message.php';
	include ROOT.'inc/_footer.php';
}else{
	header('Location: register.php');
}
?>

