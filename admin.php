<?php
include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="admin.php" class="current">Admin</a></div>
    <h1>Admin</h1>
  </div>

  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
     <p align="center">
      <a href="tambah_admin.php" type="button" class="btn btn-primary"><i class="icon-plus"></i></a><br><br>
     </p>
     <p align="right">
      <a href="export/export_excel_admin.php" type="button" class="btn btn-success">Export Excel</a>
      <a href="export/cetak_admin.php" type="button" class="btn btn-danger">Cetak</a>
    </p>
     <div class="widget-box">
       <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
        <h5>Table</h5>
      </div>
      <table class="table table-striped table-bordered table-responsive data-table">
        <thead>
          <tr>
            <th>No</th>
            <th>Username</th>
            <th>Password</th>
            <th>Email</th>
            <th>Nama Petugas</th>
            <th>Status</th>
            <th>Level</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no=1;
          $ad=mysql_query("SELECT * FROM petugas LEFT JOIN level ON petugas.id_level=level.id_level ");
          while($min=mysql_fetch_array($ad)) {
            echo "<tr>
            <td class='text-center'>$no</td>
            <td class='text-center'>$min[username]</td>
            <td class='text-center'>$min[password]</td>
            <td class='text-center'>$min[email]</td>
            <td class='text-center'>$min[nama_petugas]</td>
            <td class='text-center'>$min[status]</td>
            <td class='text-center'>$min[nama_level]</td>
            <td class='text-center'>
            <a href='edit_admin.php?id_petugas=$min[id_petugas]' class='btn btn-info'><i class='fa fa-edit'></i> Edit</a>
            <a href='hapus_admin.php?id_petugas=$min[id_petugas]' class='btn btn-danger'><i class='fa fa-trash'></i> Hapus</a>
            </td>
            </tr>";$no++;
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>



<!--End-Action boxes-->    
<?php
include"include/footer.php";
?>