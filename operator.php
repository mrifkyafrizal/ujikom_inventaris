<?php
  include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="operator.php" class="current">Operator</a></div>
    <h1>Operator</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <p align="center">
        <a href="tambah_operator.php" type="button" class="btn btn-primary"><i class="icon-plus"></i></a><br><br>
      </p>
      <p align="right">
        <a href="export/export_excel_operator.php" type="button" class="btn btn-success">Export Excel</a>
        <a href="export/cetak_operator.php" type="button" class="btn btn-danger">Cetak</a>
      </p>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
          <h5>Table</h5>
        </div>
      <table class="table table-striped table-bordered table-responsive data-table">
        <thead>
          <tr>
            <th>No</th>
            <th>Nip</th>
            <th>Nama Pegawai</th>
            <th>Alamat</th>
            <th>Level</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $no=1;
            $ad=mysql_query("SELECT * FROM pegawai LEFT JOIN level ON pegawai.id_level=level.id_level ");
            while($min=mysql_fetch_array($ad)) {
              echo "<tr>
                      <td class='text-center'>$no</td>
                      <td class='text-center'>$min[nip]</td>
                      <td class='text-center'>$min[nama_pegawai]</td>
                      <td class='text-center'>$min[alamat]</td>
                      <td class='text-center'>$min[nama_level]</td>
                      <td class='text-center'>
                        <a href='edit_operator.php?id_pegawai=$min[id_pegawai]' class='btn btn-info'><i class='fa fa-edit'></i> Edit</a>
                        <a href='hapus_operator.php?id_pegawai=$min[id_pegawai]' class='btn btn-danger'><i class='fa fa-trash'></i> Hapus</a>
                      </td>
                    </tr>";$no++;
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>


<!--End-Action boxes-->    
<?php
  include"include/footer.php";
?>