<?php
  include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="siswa.php" class="current">Siswa</a></div>
    <h1>Siswa</h1>
  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <p align="center">
        <a href="tambah_siswa.php" type="button" class="btn btn-primary"><i class="icon-plus"></i></a><br><br>
      </p>
      <p align="right">
        <a href="export/export_excel_siswa.php" type="button" class="btn btn-success">Export Excel</a>
        <a href="export/cetak_siswa.php" type="button" class="btn btn-danger">Cetak</a>
      </p>
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
          <h5>Table</h5>
        </div>
      <table class="table table-striped table-bordered table-responsive data-table">
        <thead>
          <tr>
            <th>No</th>
            <th>NISN</th>
            <th>Nama Siswa</th>
            <th>Kelas</th>
            <th>Level</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $no=1;
            $ad=mysql_query("SELECT * FROM peminjam LEFT JOIN level ON peminjam.id_level=level.id_level");
            while($min=mysql_fetch_array($ad)) {
              echo "<tr>
                      <td class='text-center'>$no</td>
                      <td class='text-center'>$min[nisn]</td>
                      <td class='text-center'>$min[nama_peminjam]</td>
                      <td class='text-center'>$min[kelas]</td>
                      <td class='text-center'>$min[nama_level]</td>
                      <td class='text-center'>
                        <a href='edit_siswa.php?id_peminjam=$min[id_peminjam]' class='btn btn-info'><i class='fa fa-edit'></i> Edit</a>
                        <a href='hapus_siswa.php?id_peminjam=$min[id_peminjam]' class='btn btn-danger'><i class='fa fa-trash'></i> Hapus</a>
                      </td>
                    </tr>";$no++;
            }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>


<!--End-Action boxes-->    
<?php
  include"include/footer.php";
?>