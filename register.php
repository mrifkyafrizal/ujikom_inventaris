<?php  
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include 'koneksi.php';

if (isset($_POST['register'])) {
    $mail = new PHPMailer(true); // Passing `true` enables exceptions

	//CREATE
	$username = $_POST["username"];
	$password = $_POST["password"];
	$re_password = $_POST['re-password'];
	$email = $_POST["email"];
	$user_key = uniqid(md5(rndStr()));
	if ($password != $re_password) {
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal, Password Tidak sama!',
		        'type' => 'error'
		    ];
	}

	$sqlCheck = "SELECT * FROM petugas WHERE username = '$username' OR email = '$email'";
	$queryCheck = mysql_query($sqlCheck);
	$row = mysql_fetch_assoc($queryCheck);
	if ($row) {
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal, Username / Email telah terpakai!',
		        'type' => 'error'
		    ];
	}
	if (empty($message)) {
		$password = password_hash($password, PASSWORD_DEFAULT);
		$sql = "INSERT INTO petugas(username,password,email,user_key) VALUES('$username','$password','$email','$user_key')";
		$query = mysql_query($sql);
	    $emailTo = $email;
		if ($query) {
		    $id = mysql_insert_id();
		    $subject = '[RPL - BM3] Activation Account For: '.$username;
		    $deskripsi = 'Link Activation: https://myprojectku.xyz/sistem_login/verify.php?id='.$id.'&user_key='.$user_key;
		    //Server settings
		    $mail->SMTPDebug = 0; // Enable verbose debug output
		    $mail->isSMTP();   // Set mailer to use SMTP
		    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true; // Enable SMTP authentication
		    $mail->Username = '';  // SMTP username
		    $mail->Password = ''; // SMTP password
		    $mail->SMTPSecure = 'ssl';  // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 465; // Ubah ke 587, bila anda menggunakan gmail

		    //Recipients
		    $mail->setFrom('no-reply@myprojectku.xyz', 'Activation Account');
		    $mail->addAddress($emailTo);     // Add a recipient
		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = $subject;
		    $mail->Body    = $deskripsi;

		    $mail->send();			
		    $message = [
		        'title' => 'Berhasil!',
		        'txt' => 'Registrasi Berhasil, silahkan cek email / folder spam anda untuk aktivasi akun!',
		        'type' => 'success'
		    ];
		}else{
		    $message = [
		        'title' => 'Gagal!',
		        'txt' => 'Registrasi Gagal!',
		        'type' => 'error'
		    ];
		}
	}
}
?>
<?php include 'include/header.php'; ?>
<div id="loginbox">        
<?php include 'inc/_message.php'; ?>    
            <form id="loginform" class="form-vertical" action="index.html">
				 <div class="control-group normal_text"> <h3><img src="img/logo.png" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="username">  value="<?= isset($username) ? $username : '' ?>" placeholder="Username" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Password" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="re-password" placeholder="Re-Password" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="email" name="email"  value="<?= isset($email) ? $email : '' ?>" placeholder="Email" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><a type="submit" href="index.html" class="btn btn-success" /> Login</a></span>
                </div>
            </form>

    </div>
<?php include 'include/footer.php'; ?>