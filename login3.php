<?php
include "koneksi.php";
session_start();
if(isset($_SESSION['nama_peminjam'])){
  header("location:index.php");
}else{
?>
<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Inventaris sekolah</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/matrix-login.css" />
        <link href="img/sekolah.png" rel="shortcut icon">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" action="proses_login3.php" method="POST">
				 <div class="control-group normal_text"><img src="img/sekolah.png" width="100px" height="50px" alt="Logo" /></div>
                 <?php
                    if(isset($_SESSION['pesan']) && $_SESSION['pesan'] <> '') {
                        echo '<div class="alert alert-danger">' .$_SESSION['pesan'].'</div>';
                    }
                    $_SESSION['pesan'] = '';
                ?>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input id="name" name="nisn" type="text" placeholder="NISN" />
                        </div>
                    </div>
                </div>
                <a href="login.php">Login sebagai petugas </a> ||
                <a href="login2.php">Login sebagai pegawai</a> ||
                <a href="help.php">Buku panduan</a>

                <div class="form-actions">
                    <span class="pull-left"><a href="forgot.php" class="flip-link btn btn-danger" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><button type="submit" class="btn btn-success" /> Login</a></span></button>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
                <p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
                
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="login.php" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><a class="btn btn-info"/>Reecover</a></span>
                </div>
            </form>
        </div>
        
        <script src="js/jquery.min.js"></script>  
        <script src="js/matrix.login.js"></script> 
    </body>

</html>
<?php
}
?>
