<?php
  include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="barang.php" class="tip-bottom">barang</a><a href="tambah_barang.php" class="current">Tambah barang</a></div>
    <h1>Tambah Barang</h1>
  </div>


  <div class="container-fluid">
    <hr>
  <div class="row-fluid">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Form Barang</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" method="POST" class="form-horizontal" enctype="multipart/form-data">
            <div class="control-group">
              <label class="control-label">File upload input</label>
              <div class="controls">
                <input type="file" name="foto" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Nama Barang :</label>
              <div class="controls">
                <input type="text" name="nama" class="span11" placeholder="Nama Barang" required>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Merk :</label>
              <div class="controls">
                <input type="text" name="merk" class="span11" placeholder="Merk barang" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Kondisi :</label>
              <div class="controls">
                <input type="text" name="kondisi" class="span11" placeholder="Kondisi" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Keterangan :</label>
              <div class="controls">
                <input type="text" name="keterangan" class="span11" placeholder="Keterangan" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Jumlah :</label>
              <div class="controls">
                <input type="text" name="jumlah" class="span11" placeholder="Jumlah" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Jenis :</label>
              <div class="controls">
                <select name="nama_jenis" class="span11" placeholder="Jenis" required />
                  <?php 
                    $sql = mysql_query("SELECT * FROM jenis");
                    while ($data=mysql_fetch_array($sql)) {
                     echo" <option value=$data[id_jenis]> $data[nama_jenis]</option> ";
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Nama Ruang :</label>
              <div class="controls">
                <select name="nama_ruang" class="span11" placeholder="Nama ruang" required />
                 <?php 
                    $sql = mysql_query("SELECT * FROM ruang");
                    while ($data=mysql_fetch_array($sql)) {
                     echo" <option value=$data[id_ruang]> $data[nama_ruang]</option> ";
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Inventaris :</label>
              <div class="controls">
                <input type="text" name="kode_inventaris" class="span11" placeholder="Inventaris" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Petugas :</label>
              <div class="controls">
                <select name="nama_petugas" class="span11" placeholder="Petugas" required />
                 <?php 
                    $sql = mysql_query("SELECT * FROM petugas");
                    while ($data=mysql_fetch_array($sql)) {
                     echo" <option value=$data[id_petugas]> $data[nama_petugas]</option> ";
                    }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-actions" style="text-align: right;">
              <a href="barang.php" type="button" class="btn btn-danger">Cancel</a>
              <input type="submit" name="submit" class="btn btn-success" value="Submit" />
            </div>
          </form>
        </div>
      </div>
  </div>
</div>
</div>
<!--End-Action boxes-->  


<?php
  if(isset($_POST['cancel']))
    echo "<script>window.location.assign('barang.php')</script>";
?>

<?php
include"./koneksi.php";
  if(isset($_POST['submit']))
{
  $foto= $_FILES['foto']['name'];
  $lokasi=$_FILES['foto']['tmp_name'];
  $upload= move_uploaded_file($lokasi, "img/".$foto);
  $nama= $_POST['nama'];
  $merk= $_POST['merk'];
  $kondisi= $_POST['kondisi'];
  $keterangan= $_POST['keterangan'];
  $jumlah= $_POST['jumlah'];
  $nama_jenis= $_POST['nama_jenis'];
  $nama_ruang= $_POST['nama_ruang'];
  $kode_inventaris= $_POST['kode_inventaris'];
  $nama_petugas= $_POST['nama_petugas'];
  $input = mysql_query("INSERT INTO inventaris (foto,nama,merk,kondisi,keterangan,jumlah,id_jenis,id_ruang,kode_inventaris,id_petugas) VALUES ('$foto','$nama','$merk','$kondisi','$keterangan','$jumlah','$nama_jenis','$nama_ruang','$kode_inventaris','$nama_petugas')");
  if($input){
    echo "<script>window.location.assign('barang.php')</script>";
  }else{
    echo "gagal";
  }
}
?>

<?php
  include"include/footer.php";
?>