<?php
  include"include/header.php";
?>
<!--Action boxes-->
<div id="content">
  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.php" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a><a href="jenis.php" class="tip-bottom">Jenis</a><a href="tambah_jenis" class="current">Tambah jenis</a></div>
    <h1>Tambah jenis</h1>
  </div>

  <div class="container-fluid">
    <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Form jenis</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" method="POST" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Nama jenis :</label>
              <div class="controls">
                <input type="text" name="nama_jenis" class="span11" placeholder="Nama jenis" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Kode jenis :</label>
              <div class="controls">
                <input type="text" name="kode_jenis" class="span11" placeholder="Kode jenis" required />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Keterangan :</label>
              <div class="controls">
                <input type="text" name="keterangan" class="span11" placeholder="Keterangan" required />
              </div>
            </div>
            <div class="form-actions" style="text-align: right;">
              <a href="jenis.php" type="button" class="btn btn-danger">Cancel</a>
              <input type="submit" name="submit" class="btn btn-success" value="Submit" />
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--End-Action boxes-->  

<?php
  if(isset($_POST['cancel']))
    echo "<script>window.location.assign('jenis.php')</script>";
?>

<?php
include"./koneksi.php";
  if(isset($_POST['submit']))
{
  $nama_jenis= $_POST['nama_jenis'];
  $kode_jenis= $_POST['kode_jenis'];
  $keterangan= $_POST['keterangan'];
  $input = mysql_query("INSERT INTO jenis (nama_jenis,kode_jenis,keterangan) VALUES ('$nama_jenis','$kode_jenis','$keterangan')");
  if($input){
    echo "<script>window.location.assign('jenis.php')</script>";
  }else{
    echo "gagal";
  }
}
?>

<?php
  include"include/footer.php";
?>
