Code By: <a href="https://rpl-bm3.com/profile/harcie">Harie</a>
	<!-- jQuery library -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
	<script>
if ($('.message').data('message')) {
	const href = $('.message').data('href');
	Swal.fire({
		title: $('.message').data('title'),
		text : $('.message').data('message'),
		type: $('.message').data('type'),

		onClose: () => {
    		window.location.assign(href)
  		}
	});
		}			
	</script>
  </body>
</html>