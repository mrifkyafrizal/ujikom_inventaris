<?php
include "koneksi.php";
session_start();
if($_SESSION['nama_admin']!='admin')
{
  header("location:login.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Inventaris Sekolah</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/select2.css" />
<link href="img/sekolah.png" rel="shortcut icon">
<!-- <link rel="stylesheet" href="css/fullcalendar.css" /> -->
<link rel="stylesheet" href="css/matrix-style.css" />
<link rel="stylesheet" href="css/matrix-media.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/style_barang.css">
<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
<!-- <link rel="stylesheet" href="css/jquery.gritter.css" /> -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<!-- <base href="localhost/ujikom/"> -->
</head>
<body>

<!--Header-part-->
<div id="header">
  <h3><a style="color: #eae2e2d9;" href="../index.php">Inventaris</a></h3>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li id="profile-messages" ><a title="" href="#"><i class="icon icon-user"></i><span class="text"> Welcome User</a>
    </li>
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--sidebar-menu-->
<div id="sidebar"><a href="index.php" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li id="dashboard"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    

    <?php if($_SESSION['akses'] == 'administrator'){?>
    <li class="submenu"> <a href="#"><i class="icon icon-group"></i> <span>Pengguna</span> <span class="label label-important">3</span></a>
      <ul>
        <li id="admin"><a href="admin.php">Admin</a></li>
        <li><a href="operator.php">Operator</a></li>
        <li><a href="siswa.php">Peminjam</a></li>
        
      </ul>
    </li>
    <li class="submenu"> <a href="#"><i class="icon icon-briefcase"></i> <span>Inventarisir</span> <span class="label label-important">3</span></a>
      <ul>
        <li><a href="barang.php">Barang</a></li>
       
        <li><a href="ruang.php">Ruang</a></li>
        <li><a href="jenis.php">Jenis</a></li>
        
      </ul>
    </li>
    <li id="peminjaman">
      <a href="peminjaman.php"><i class="icon icon-inbox"></i><span>Peminjaman</span></a>
    </li>
   <li class="submenu"> <a href="#"><i class="icon icon-briefcase"></i> <span>Pengembalian</span> <span class="label label-important">3</span></a>
      <ul>
        <li><a href="pengembalian.php">Pengembalian</a></li>
        <li><a href="pengembalian_op.php">Pengembalian OP</a></li>        
        <li><a href="pengembalian_siswa.php">Pengembalian Siswa</a></li>        
      </ul>
    </li>
    <li>
      <a href="backup_database.php"><i class="icon icon-refresh"></i><span>Backup Database</span></a>
    </li>
    <?php } ?>

    <?php if ($_SESSION['akses']=='operator'){ ?>
        <li>
          <a href="peminjaman_op.php"><i class="icon icon-inbox"></i><span>Peminjaman</span></a>
        </li>
        <li>
          <a href="pengembalian_op.php"><i class="icon icon-envelope"></i><span>Pengembalian</span></a>
        </li>
    <?php } ?>
    <?php if ($_SESSION['akses']=='peminjam'){ ?>
       <li>
          <a href="peminjaman_siswa.php"><i class="icon icon-inbox"></i><span>Peminjaman</span></a>
        </li>
    <?php } ?>
  </ul>
</div>
<!--sidebar-menu-->



